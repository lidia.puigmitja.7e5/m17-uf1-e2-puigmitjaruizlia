﻿using M17_UF1_E2_PuigmitjaRuizLia;
using System;
using System.ComponentModel.Design;
using System.Numerics;

namespace GameTools
{

    /*
     * GameEngine:
     * Protype to print sequence of frames in console.
     * 
     * To stop de main while press ESC key.
     * 
     */


    public abstract class GameEngine
    {

        //Declaració d'una variable
        private ConsoleColor _backgroundConsoleColor;
        //Declaració d'una propietat
        public ConsoleColor BackgroundConsoleColor
        {
            get { return _backgroundConsoleColor; }
            set
            {
                if (ConsoleColor.White != value || ConsoleColor.Black != value) _backgroundConsoleColor = value;
                else
                {
                    _backgroundConsoleColor = ConsoleColor.Gray;
                    throw new ArgumentException($"Console color {value} not recomended. Set by default");
                }
            }
        }

        private int time2liveframe;
        private float _frameRate;
        public float FrameRate
        {
            get { return _frameRate; }
            set
            {
                //Ternari condition (Si valor<0 -> valor*-1, Si no ho es-> retorna valor)
                _frameRate = (value < 0f) ? value * (-1f) : value;
            }
        }


        //Declaració d'una propietat no protegida
        public int Frames { get; set; }

        protected ConsoleKeyInfo cki;
        private bool engineSwitch;

        public GameEngine()
        {
            Menu();
            
        }
     
            public void Menu()
            {
            Console.SetWindowSize(35, 25);
            bool fi;
                do
                {
                    MostrarMenu();
                    fi = TractarOpcio();
                } while (!fi);
            }

            private void MostrarMenu()
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\n");
                Console.WriteLine("▀█▄░▄█░░░█▄▄▐▀█▀▌▐▀█░▀█░▀█▄░▐▀░");
                Console.WriteLine("░█▐█▀▐░░▐▄██░░▌░░▐▄▌░░▌░░░██▀░░");
                Console.WriteLine("░█░▌░▐░░▌▀██░░▌░░▐▀▄░░▌░▐▀░▀█▄░");
                Console.WriteLine("▄▌░░░▀░▀░░▀▀░▀▀▀░▀░▀▀░▀░▀░░░░▀▀");
                Console.WriteLine("\n");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("[1] Red Pill: Jugar amb cursor");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("[2] Blue Pill: Jugar Automatic");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("[0] Acabar amb tot");
            }

            private bool TractarOpcio()
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Escriu la opció:");
                var opcio = Console.ReadLine();
                var final = false;

                switch (opcio)
                {
                    case "1":
                    InitGame();
                    UpdateGameManual();
                    CloseGame();
                        Console.ReadKey();
                        break;

                    case "2":
                    InitGame();
                    UpdateGameAleatori();
                    CloseGame();
                        Console.ReadKey();
                        break;


                    case "0":
                        final = true;
                        break;

                    default:
                        Console.WriteLine("Opció incorrecta!\n");
                        break;
                }

                return final;
            }



        


        private void InitGame()
        {
            /*** Init variables ***/

            Frames = 0;
            engineSwitch = false;

            //Acces exemple with this:
            this._frameRate = (_frameRate <= 0) ? 12 : _frameRate;

            //Calculate the frame time in miliseconds. Time to refresh. F=1/s ->s=1/F
            time2liveframe = (int)((1 / _frameRate) * 1000);

            /*******/

            //Prepare Console
            CleanFrame();
            Console.BackgroundColor = _backgroundConsoleColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nPress a key to display; \npress the ESC key to quit.");

            Console.WriteLine($"Game Initiation             \nRender data: Framerate: {_frameRate}  \nTimeToRefresh:{time2liveframe}");

            Start();

            System.Threading.Thread.Sleep(2000);
        }

        /*
         * Engine updates every frame
         * 
         * Reprint console
         */
        private void UpdateGameManual()
        {

            do
            {

                while (Console.KeyAvailable == false)
                {
                   
                    CleanFrame();

                    UpdateManual();

                  //  Console.WriteLine(engineSwitch);

                    RefreshFrame();

                    Frames++;
                }
                
                cki = Console.ReadKey(true);
                
                CheckKeyboard4Engine();

            } while (engineSwitch);

        }

        private void UpdateGameAleatori()
        {

            do
            {

                while (Console.KeyAvailable == false)
                {

                    CleanFrame();

                    UpdateAutomatic();

                    //  Console.WriteLine(engineSwitch);

                    RefreshFrame();

                    Frames++;
                }

                cki = Console.ReadKey(true);

                CheckKeyboard4Engine();

            } while (engineSwitch);

        }


        private void ListenKeyboard()
        {
            cki = Console.ReadKey();
        }

        private void CheckKeyboard4Engine()
        {
            engineSwitch = (cki.Key != ConsoleKey.Escape);
        }

        private void RefreshFrame()
        {
            //Access to Threading library only in this line
            System.Threading.Thread.Sleep(time2liveframe);
        }

        private void CloseGame()
        {
            Console.Clear();
            Console.WriteLine("You pressed the '{0}' key.", cki.Key);
            Exit();
            Console.WriteLine(" Game Over. Closing game");
            Console.ReadKey();
            
        }


        private void CleanFrame()
        {
            Console.Clear();
            Console.SetCursorPosition(Console.CursorLeft, Console.CursorSize);
        }

        protected abstract void Start();

        //Code before first frame



        protected abstract void UpdateAutomatic();

        protected abstract void UpdateManual();

        //Execution ontime secuence of every frame


        protected abstract void Exit();

        //Code afer last frame

      

    }
}


public class ConsoleSpiner
{
    int counter;
    public ConsoleSpiner()
    {
        counter = 0;
    }
    public void Turn()
    {
        counter++;
        switch (counter % 4)
        {
            case 0: Console.Write("/"); break;
            case 1: Console.Write("-"); break;
            case 2: Console.Write("\\"); break;
            case 3: Console.Write("|"); break;
        }
        //Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
    }
}
