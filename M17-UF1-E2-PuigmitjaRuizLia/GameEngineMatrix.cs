﻿using GameTools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.Design;
using System.Reflection.Emit;
using System.Runtime.Intrinsics.X86;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml.Linq;

namespace M17_UF1_E2_PuigmitjaRuizLia
{
    internal class GameEngineMatrix : GameEngine
    {
        MatrixRepresentation matrixRepresentation = new MatrixRepresentation(20, 30);
        int[,] matrixIndice = new int[20, 30];
        Random random = new Random();
        int cursorX=0;


        public GameEngineMatrix() : base()
        {
           

        }
       
        protected override void Start()
        {
            //Code before first frame
            Console.ForegroundColor = ConsoleColor.Green;
            BackgroundConsoleColor = ConsoleColor.Black;
            FrameRate = 99999f;
            Console.CursorVisible = false;

           

            matrixRepresentation.CleanTheMatrix();
            

        }


        protected void ControlCursor(ConsoleKeyInfo cki, char[,] aux)
        {
            
                    switch (cki.Key)
                    {
                        case ConsoleKey.LeftArrow:
                            if (cursorX == 0) break;
                            cursorX--;
                            break;

                        case ConsoleKey.RightArrow:
                            if (cursorX == matrixIndice.GetLength(1) - 1) break;
                            cursorX++;
                            break;

                        default:
                            break;
            


            }


            matrixRepresentation.CleanTheMatrix();
            for(int i=0; i < matrixIndice.GetLength(0); i++)
            {
                for(int j=0; j < matrixIndice.GetLength(1); j++)
                {
                    aux[i, j] = ' ';
                    matrixIndice[i,j] = 0;
                }
            }
            aux = matrixRepresentation.TheMatrix;

            
            matrixRepresentation.printMatrix(matrixIndice, cursorX);



        }

        //Genera columnes aleatories i lletres aleatories
        protected override void UpdateAutomatic()
        {
            char[,] aux = matrixRepresentation.TheMatrix;
            int aleatori;


            do
                {
                    aleatori = ColumnaAleatoria();

                } while (aux[0, aleatori] != ' ');
            
                aux[0, aleatori] = Randomissimo();
            matrixIndice[0, aleatori] = 5;

            RecorrerMatrix(aux);
        

        }

        //Mous el cursor i prems enter on vulguis que surti la primera fila, les altres surten aleatoriament.
        //Pots tornar a moure el cursor i premer enter els cops que vulguis, es netejarà la matriu i tornarà a caure per la columna que vulguis.
        protected override void UpdateManual()
        {
            
            char[,] aux = matrixRepresentation.TheMatrix;

            while (cki.Key != ConsoleKey.Enter)
            {
                cki = Console.ReadKey(true);
                ControlCursor(cki, aux);
                aux[0, cursorX] = Randomissimo();
                matrixIndice[0, cursorX] = 5;
            }

            matrixRepresentation.CleanTheMatrix();
            
            

            RecorrerMatrix(aux);
            UpdateAutomatic();
            

        }


       

        protected void RecorrerMatrix(char[,] aux){
            for (int x = matrixRepresentation.TheMatrix.GetLength(0) - 1; x > 0; x--)
            {
                for (int i = matrixRepresentation.TheMatrix.GetLength(1) - 1; i >= 0; i--)
                {

                    if (aux[x - 1, i] != 0)
                    {
                        aux[x, i] = aux[x - 1, i];
                        matrixIndice[x, i]= matrixIndice[x-1, i];

                        if(matrixIndice[x - 1, i] > 1)
                        {
                            matrixIndice[x - 1, i]--;
                        }
                        

                        if (x == 1 && aux[x - 1, i] != ' ')
                        {
                            aux[x - 1, i] = Randomissimo();

                        }


                        if (aux[matrixRepresentation.TheMatrix.GetLength(0) / 3, i] != ' ')
                        {
                            aux[0, i] = ' ';
                            matrixIndice[0, i] = 0;
                        }
                       
                    }
                    


                }
            }
                    matrixRepresentation.clippingMatrix(aux);
                    matrixRepresentation.printMatrix(matrixIndice, cursorX);
                    System.Threading.Thread.Sleep(50);
            
        }
       


        protected char Randomissimo()
        {
            
            string caracters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            char letra;
            return caracters[random.Next(caracters.Length)];
        }

        protected int ColumnaAleatoria()
        {
           return random.Next(0, matrixRepresentation.TheMatrix.GetLength(1));
        }

        protected override void Exit()
        {
            
            Console.WriteLine("─────────────────────────────────");
Console.WriteLine("───────────────▄████████▄────────");
Console.WriteLine("─────────────▄█▀▒▒▒▒▒▒▒▀██▄──────");
Console.WriteLine("───────────▄█▀▒▒▒▒▒▒▒▒▒▒▒██──────");
Console.WriteLine("─────────▄█▀▒▒▒▒▒▒▄▒▒▒▒▒▒▐█▌─────");
Console.WriteLine("────────▄█▒▒▒▒▒▒▒▒▀█▒▒▒▒▒▐█▌─────");
Console.WriteLine("───────▄█▒▒▒▒▒▒▒▒▒▒▀█▒▒▒▄██──────");
Console.WriteLine("──────▄█▒▒▒▒▒▒▒▒▒▒▒▒▀█▒▄█▀█▄─────");
Console.WriteLine("─────▄█▒▒▒▒▒▒▒▒▒▒▒▒▒▒██▀▒▒▒█▄────");
Console.WriteLine("────▄█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄───");
Console.WriteLine("───▄█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▄──");
Console.WriteLine("──▄█▒▒▒▄██████▄▒▒▒▒▄█████▄▒▒▒▒█──");
Console.WriteLine("──█▒▒▒█▀░░░░░▀█─▒▒▒█▀░░░░▀█▒▒▒█──");
Console.WriteLine("──█▒▒▒█░░▄░░░░▀████▀░░░▄░░█▒▒▒█──");
Console.WriteLine("▄███▄▒█▄░▐▀▄░░░░░░░░░▄▀▌░▄█▒▒███▄");
Console.WriteLine("█▀░░█▄▒█░▐▐▀▀▄▄▄─▄▄▄▀▀▌▌░█▒▒█░░▀█");
Console.WriteLine("█░░░░█▒█░▐▐──▄▄─█─▄▄──▌▌░█▒█░░░░█");
Console.WriteLine("█░▄░░█▒█░▐▐▄─▀▀─█─▀▀─▄▌▌░█▒█░░▄░█");
Console.WriteLine("█░░█░█▒█░░▌▄█▄▄▀─▀▄▄█▄▐░░█▒█░█░░█");
Console.WriteLine("█▄░█████████▀░░▀▄▀░░▀█████████░▄█");
Console.WriteLine("─██▀░░▄▀░░▀░░▀▄░░░▄▀░░▀░░▀▄░░▀██");
            Console.WriteLine("Pa que tocas nah!? HAHAHAHAHHAHA!");
        }
    }
}
